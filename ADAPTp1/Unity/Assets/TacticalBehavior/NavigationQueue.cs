using UnityEngine;
using TreeSharpPlus;
using System.Collections.Generic;

/// <summary>
/// A queue of points representing a path through the environment allowing for
/// smooth movement by advancing the target before the agent reaches it. -KN
/// </summary>
public class NavigationQueue
{
    const float ContinueDistance = 1f;

    public event System.EventHandler OnUpdate;

    bool infinite;
    NavWaypoint destination;
    public Queue<NavWaypoint> waypoints = new Queue<NavWaypoint>();
    NavWaypoint lastDequeued;

    public bool Ready { get; set; }

    public NavigationQueue copy()
    {
        NavigationQueue newQueue = new NavigationQueue(infinite);
        newQueue.destination = destination;
        newQueue.waypoints = new Queue<NavWaypoint>(waypoints);
        return newQueue;
    }

    public bool IsValid {
        get { return waypoints != null; }
    }

    public bool IsEmpty {
        get { return waypoints.Count == 0; }
    }

    /// <summary>
    /// Returns the waypoint that the agent should be moving toward.
    /// </summary>
    Val<NavWaypoint> TargetWaypoint {
        get { return Val.Val(() => IsEmpty ? destination : waypoints.Peek()); }
    }

    /// <summary>
    /// Gets the position of the current target.
    /// </summary>
    public Val<Vector3> TargetPoint {
        get { return Val.Val(() => TargetWaypoint.Value.Point); }
    }

    /// <summary>
    /// Gets the stopping distance from the current target. This is zero for
    /// all but the final destination. It's also zero for the destination if
    /// the queue is in infinite mode (so that the behavior tree never ends).
    /// </summary>
    public Val<float> TargetStopDist {
        get { return Val.Val(() =>
            (IsEmpty && !infinite) ? TargetWaypoint.Value.Radius : 0f); }
    }

    public NavigationQueue(bool infinite) {
        this.infinite = infinite;
    }

    public void NewPlan(bool ready, IEnumerable<DefaultState> states, DefaultState dst)
    {
        Ready = ready;
        destination = new NavWaypoint() {
            Point = dst.statePosition(),
            Radius = PathNav.CloseEnough
        };
        waypoints.Clear();
        foreach (var s in states) {
            var w = new NavWaypoint() {
                Point = s.statePosition(),
                Radius = ContinueDistance
            };
            waypoints.Enqueue(w);
        }
    }

#if false
    /// <summary>
    /// Creates a new <see cref='NavigationQueue'/> from src to dst along perim.
    /// </summary>
    public NavigationQueue(
        PerimeterDefinition perim, Transform src, NavWaypoint dst)
    {
        //perimeter = perim;
        //source = src;
        destination = dst;
        waypoints = Along(perim, src, dst);
    }

    /// <summary>Wraps i to [0, c).</summary>
    private static int Wrap(int i, int c) {
        return (i % c + c) % c;
    }

    /// <summary>
    /// Constructs a path between two <see cref='Transform'/>s along a
    /// <see cref='PerimeterDefinition'/>.
    /// </summary>
    private static Queue<NavWaypoint> Along(PerimeterDefinition perimeter,
        Transform source, NavWaypoint destination)
    {
        var perim = perimeter.perimeter;
        int first = 0, last = 0;
        float firstmin = float.PositiveInfinity;
        float lastmin = float.PositiveInfinity;
        for (int i = 0; i < perim.Count; i++) {
            var t = perim[i];
            if (t == null) continue;

            float firstval = (source.position - t.position).sqrMagnitude;
            if (firstval < firstmin) {
                first = i;
                firstmin = firstval;
            }

            float lastval = (destination.Point.Value - t.position).sqrMagnitude;
            if (lastval < lastmin) {
                last = i;
                lastmin = lastval;
            }
        }

        var c = perim.Count;

        var forward = new Queue<NavWaypoint>();
        int forwardcount = 0;
        for (int i = first; i < first + c; i++) {
            int ii = Wrap(i, c);
            var p = perim[ii];
            forwardcount++;
            if (p == null) {
                forward = null;
                break;
            }
            if (!p.CompareTag("PerimeterUnnavigable")) {
                forward.Enqueue(perim[ii]);
            }
            if (ii == last) break;
        }

        var backward = new Queue<NavWaypoint>();
        int backwardcount = 0;
        for (int i = first; i > first - c; i--) {
            int ii = Wrap(i, c);
            var p = perim[ii];
            backwardcount++;
            if (p == null) {
                backward = null;
                break;
            }
            if (!p.CompareTag("PerimeterUnnavigable")) {
                backward.Enqueue(perim[ii]);
            }
            if (ii == last) break;
        }

        if (forward == null) return backward;
        if (backward == null) return forward;
        return forwardcount < backwardcount ? forward : backward;
    }
#endif

    /// <summary>
    /// Updates the queue: dequeues a waypoint if necessary.
    /// </summary>
    public void Update(Vector3 agentPos)
    {
        /*
        waypoints = Along();
        if (!IsValid) {
            throw new System.ArgumentException(string.Format(
                "Could not find a path to {0} along {1}.",
                this.destination, this.perimeter));
        }
        */
        // - KN

        if (OnUpdate != null) {
            OnUpdate(null, null);
        }

        if (waypoints.Count > 0 && waypoints.Peek().Equals(lastDequeued)) {
            waypoints.Dequeue();
        }

        NavWaypoint t;
        while (waypoints.Count > 0 &&
            Vector3.Distance(agentPos, (t = waypoints.Peek()).Point) <=
                t.Radius) {
            lastDequeued = waypoints.Dequeue();
        }
    }
}
