using UnityEngine;
using System.Collections;
using TreeSharpPlus;

public class WalkTo : Behavior
{
    public Transform target;
    public bool allowWalk;

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Q)) {
        if (allowWalk) {
            allowWalk = !allowWalk;
            GetComponent<UnitySteeringController>().maxSpeed =
                Input.GetKey(KeyCode.LeftShift) ? 0f : 2.2f;
            base.StartTree(
                new Sequence(
                    this.Node_GoTo(target.position, 1f),
                    new DecoratorLoop(new LeafWait(10))
                    )
                );
        }
    }
}
