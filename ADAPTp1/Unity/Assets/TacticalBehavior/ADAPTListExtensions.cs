using UnityEngine;
using TreeSharpPlus;
using System.Collections.Generic;
using System.Linq;

static class ADAPTListExtensions {
    public static R[] Map<T, R>(this IEnumerable<T> list, System.Func<T, R> f)
    {
        return list.Select(f).ToArray();
    }

    public static Node MapSequence<T>(this IEnumerable<T> list, System.Func<T, Node> f)
    {
        return new Sequence(list.Map(f));
    }

    public static Node MapSequenceParallel<T>(this IEnumerable<T> list, System.Func<T, Node> f)
    {
        return new SequenceParallel(list.Map(f));
    }

    public static Node MapSequenceShuffle<T>(this IEnumerable<T> list, System.Func<T, Node> f)
    {
        return new SequenceShuffle(list.Map(f));
    }
}
