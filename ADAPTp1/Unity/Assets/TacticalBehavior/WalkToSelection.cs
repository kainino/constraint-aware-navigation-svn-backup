using UnityEngine;
using System.Collections;

public class WalkToSelection : MonoBehaviour {

    GameObject selectedAgent;
    GameObject[] patrolingAgents;
    Vector3[] previousPositions;
    public static bool AgentsWalking = false;
    public Transform target;
    public bool allowwalking = false;

	// Use this for initialization
	void Start () {

        patrolingAgents = GameObject.FindGameObjectsWithTag("Agent");
        previousPositions = new Vector3[patrolingAgents.Length];
	}
	
	// Update is called once per frame
	void Update () {

        setAgentWalking();
        if (allowwalking) {
            allowwalking = !allowwalking;
            GetComponent<NavMeshAgent>().destination = target.position;

        }

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f)) {
                Transform test = hit.transform;
                do {
                    if (selectedAgent != null && test.gameObject.tag == "Waypoint") {
                        selectedAgent.GetComponent<NavMeshAgent>().destination = test.position;
                        Debug.Log("Waypoint Selected");
                        break;
                    } else if (test.gameObject.tag == "Agent") {
                        selectedAgent = test.gameObject;
                        Debug.Log("Agent Selected");
                        break;
                    }
                    test = test.parent;
                } while(test != null);
            }
        }
	}

    void setAgentWalking()
    {
        AgentsWalking = false;
        for (int i = 0; i < patrolingAgents.Length; i++)
        {
            if (Vector3.Distance(previousPositions[i], patrolingAgents[i].transform.position) > 0.001f)
            {
                AgentsWalking = true;
                break;
            }
        }

        for (int j = 0; j < patrolingAgents.Length; j++)
        {
            previousPositions[j] = patrolingAgents[j].transform.position;
        }
    }

}
