using UnityEngine;
using System.Collections;

public class Annotations : MonoBehaviour {
    public BoxCollider Near;
    public BoxCollider Back;
    public BoxCollider Front;
    public BoxCollider Left;
    public BoxCollider Right;
}