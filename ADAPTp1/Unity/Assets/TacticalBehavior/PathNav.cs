using System.Collections.Generic;
using UnityEngine;
using TreeSharpPlus;

/// <summary>
/// Static methods for path navigation.
/// </summary>
public static class PathNav
{
    /// <summary>
    /// Distance we need to be "close enough" to something to be near it.
    /// </summary>
    public const float CloseEnough = 1f;

    /// <summary>
    /// Gets the center of a Transform (either the collider center or the
    /// transform position).
    /// </summary>
    public static Val<Vector3> GetCenter(Transform other)
    {
        return Val.Val(() =>
            other.collider != null
            ? other.collider.bounds.center : other.transform.position);
    }

    /// <summary>
    /// Gets the diameter of a Transform: how close we need to get to be
    /// "near"/"next to" an object.
    /// </summary>
    public static Val<float> GetDiameter(Val<Transform> other)
    {
        return Val.Val(() =>
            other.Value.collider == null ? CloseEnough
            : Mathf.Sqrt(
                Mathf.Pow(other.Value.collider.bounds.extents.x, 2) +
                Mathf.Pow(other.Value.collider.bounds.extents.z, 2))
            );
    }

    /// <summary>
    /// Builds and returns a subtree for navigating along a path, then waiting.
    /// </summary>
    /// <param name='destination'>
    /// The final destination of the path.
    /// </param>
    /// <param name='waypoints'>
    /// The waypoints along the path (excluding <paramref name='destination'/>).
    /// </param>
    public static Node BuildTree(Behavior b, NavigationQueue queue, float speed)
    {
        var steering = b.GetComponent<UnitySteeringController>();

        // The order here is important! Update, then tick the GoTo node. - KN
        return new Race(
            new DecoratorLoop(
                new LeafInvoke(() => {
                    queue.Update(b.transform.position);
                    steering.SlowArrival = queue.IsEmpty;
                    steering.maxSpeed = queue.Ready ? speed : 0f;
                })
                ),
            b.Node_GoTo(queue.TargetPoint, queue.TargetStopDist)
            );
    }
}
