using UnityEngine;
using TreeSharpPlus;

/// <summary>
/// Represents a navigation waypoint.
/// </summary>
public struct NavWaypoint
{
    /// <summary>
    /// The radius at which the <see cref='NavigationQueue'/> dequeues this waypoint.
    /// </summary>
    public float Radius { get; set; }
    /// <summary>
    /// The location of the waypoint.
    /// </summary>
    public Vector3 Point { get; set; }

    public bool Near(Vector3 other)
    {
        return (Point - other).magnitude < Radius;
    }

    public override bool Equals(object o)
    {
        return o is NavWaypoint && Equals((NavWaypoint) o);
    }

    public bool Equals(NavWaypoint o)
    {
        return Radius == o.Radius && Point == o.Point;
        //return Mathf.Abs(Radius - o.Radius) < 0.001f &&
        //    Vector3.Distance(Point, o.Point) < 0.001f;
    }

    public override int GetHashCode()
    {
        return Radius.GetHashCode() ^ Point.GetHashCode();
    }
}
