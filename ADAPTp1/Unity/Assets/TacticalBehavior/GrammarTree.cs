using UnityEngine;
using TreeSharpPlus;

#if false
public static class GrammarTree
{
    public static Node Create(TacBeh a)
    {
        Val<Vector3> center = PathNav.GetCenter(a.GoalObj);
        Val<float> diam = PathNav.GetDiameter(a.GoalObj);

        if (a.Action == TacBeh.Verb.Move) {
            NavWaypoint destination;
            if (a.Goal == TacBeh.GoalPrep.To) {
                destination = new NavWaypoint() {
                    Point = center, Radius = diam };
            } else {
                var marker = a.GoalObj.Find(a.Goal.ToString());
                if (marker == null) {
                    throw new System.ArgumentException(string.Format(
                        "Cardinal direction marker {0} not found on {1}.",
                        a.Goal, a.GoalObj));
                }
                destination = new NavWaypoint() {
                    Point = marker.position,
                    Radius = PathNav.CloseEnough };
            }

            if (a.Constr == TacBeh.ConstrPrep.Near) {
                var perim = a.ConstrObj.GetComponent<PerimeterDefinition>();
                if (perim == null) {
                    throw new System.ArgumentException(string.Format(
                        "No PerimeterDefinition component found on {0}.",
                        a.ConstrObj));
                }
                // TODO: Does not deal with the case where the result of
                // perim.Along is not accessible.
                // We had discussed on 2013-05-28 the possibility of cancelling
                // an action if we get stuck on it for too long. Perhaps this
                // is the way to go. - KN
                var queue = new NavigationQueue(
                    perim, a.Subj.transform, destination);
                return PathNav.BuildTree(a.Subj, queue);
            } else {
                // TODO: Pretends the target is round. - KN
                return a.Subj.Node_GoTo(destination.Point, destination.Radius);
            }
        }

        throw new System.ArgumentException(
            "The TacBeh could not be translated into a behavior tree.");
    }
}
#endif
