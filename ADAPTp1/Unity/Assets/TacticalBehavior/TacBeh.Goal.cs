using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public partial struct TacBeh {
    [System.Serializable]
    public class Goal {
        public GoalPrep prep;
        public Cardinal dir;
        public Transform obj;

        public BoxCollider Annotation
        {
            get {
                var o = obj.GetComponent<Annotations>();
                if (o == null) return obj.collider as BoxCollider;
    
                if (prep == GoalPrep.To) {
                    if (dir == Cardinal.None) return o.Near;
                    else if (dir == Cardinal.BackOf) return o.Back;
                    else if (dir == Cardinal.FrontOf) return o.Front;
                    else if (dir == Cardinal.LeftOf) return o.Left;
                    else if (dir == Cardinal.RightOf) return o.Right;
                }
    
                return o.collider as BoxCollider;
            }
        }

        public override string ToString()
        {
            return string.Format("({0} {1} {2})",
                prep, dir == Cardinal.None ? "" : dir.ToString(), obj.name);
        }
    }
}
