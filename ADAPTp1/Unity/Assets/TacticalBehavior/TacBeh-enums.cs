using UnityEngine;
using System.Collections;

public partial struct TacBeh {
    public enum Verb
    {
        Move,
        LookAt,
    }

    public enum Adverb
    {
        None,
        Quickly,
        Slowly,
        Covertly,
    }

    public enum ConstrPrep
    {
        None,
        In,
        Near,
    }

    public enum GoalPrep
    {
        To,
    }

    public enum Cardinal
    {
        None,
        BackOf,
        FrontOf,
        LeftOf,
        RightOf,
    }
}
