using UnityEngine;
using System.Linq;

/// <summary>
/// SUBJ, ACTION MODE [CONSTR CONSTROBJ] GOAL GOALOBJ
/// </summary>
public partial struct TacBeh
{
    public const float MultiplierBase = 5.0f;

    public Behavior Subj { get; set; }

    public Verb Action { get; set; }

    public Adverb Mode { get; set; }

    public Constraint[] Constrs { get; set; }

    public Goal Goals { get; set; }

    public override string ToString()
    {
        return string.Format(
            "[TacBeh: {0}, {1} {2} {3} {4}]",
            Subj.name, Action,
            Mode == Adverb.None ? "" : Mode.ToString(),
            string.Format("({0})",
                string.Join(", ", Constrs.Select(c => c.ToString()).ToArray())),
            Goals);
    }

    public BoxCollider SubjAnnotation
    {
        get {
            var o = Subj.GetComponent<Annotations>();
            if (o == null) return Subj.collider as BoxCollider;
            return o.Near;
        }
    }

    public float Speed
    {
        get {
            if (Mode == Adverb.Quickly) return 5.7f; // running
            else if (Mode == Adverb.Slowly) return 1f; // walking
            else return 2.2f;
        }
    }

    public float MultiplierField(Vector3 mp)
    {
        // This is where we change the base multiplier
        // (i.e. the multiplier when there are no constraints).
        float ret = MultiplierBase;
        if (Constrs != null) {
            foreach (var c in Constrs) {
                ret += c.MultiplierField(mp);
            }
        }
        return Mathf.Max(1f, ret); //Mathf.Max(1f, ret);
    }
}
