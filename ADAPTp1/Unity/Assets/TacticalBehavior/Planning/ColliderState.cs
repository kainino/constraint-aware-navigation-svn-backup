using UnityEngine;
using System.Collections.Generic;

public class ColliderState : NavGraphNode
{
    public BoxCollider Coll { get; private set; }

    public ColliderState(BoxCollider c)
    {
        Coll = c;
    }

    public ColliderState(BoxCollider c, IEnumerable<NavGraphNode> nodes)
        : this(c)
    {
        Adjacencies.UnionWith(nodes);
        //foreach (var n in nodes) {
        //    Adjacencies.UnionWith(n.Adjacencies);
        //}
    }

    public override Vector3 Position {
        get { return Coll.transform.position; }
    }

    public static float Distance(DefaultState s, DefaultState goal) {
        Vector3 closest;
        var g = goal as ColliderState;
        if (g != null) {
            closest = g.Coll.ClosestPoint(s.statePosition());
        } else {
            closest = goal.statePosition();
        }
        return Vector3.Distance(s.statePosition(), closest);
    }
}
