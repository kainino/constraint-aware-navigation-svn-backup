using UnityEngine;
using System.Collections.Generic;

public abstract class NavGraphNode : DefaultState
{
    public HashSet<NavGraphNode> Adjacencies { get; private set; }
    //public HashSet<GameObject> Annotations { get; private set; }

    //public NavGraphNode(IEnumerable<GameObject> annotations)
    public NavGraphNode()
    {
        Adjacencies = new HashSet<NavGraphNode>();
        //Annotations = new HashSet<GameObject>();
        //Annotations.UnionWith(annotations);
    }

    public abstract Vector3 Position { get; }

    public override Vector3 statePosition()
    {
        return Position;
    }
}
