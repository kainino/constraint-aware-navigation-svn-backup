using UnityEngine;
using System.Collections;

public static class ExtensionsBoxCollider
{
    public static bool Contains(this BoxCollider bc, Vector3 p)
    {
        return bc.Contains(p, 0f);
    }

    public static bool Contains(this BoxCollider bc, Vector3 p, float d)
    {
        return bc.MinDistance(p) < d;
    }

    public static float MinDistance(this BoxCollider bc, Vector3 p)
    {
        return Vector3.Distance(bc.ClosestPoint(p), p);
    }

    public static Vector3 ClosestPoint(this BoxCollider bc, Vector3 p)
    {
        var t = bc.transform;
        var p2 = t.InverseTransformPoint(p);
        float xmin = bc.center.x - bc.size.x / 2,
              xmax = bc.center.x + bc.size.x / 2,
              ymin = bc.center.y - bc.size.y / 2,
              ymax = bc.center.y + bc.size.y / 2,
              zmin = bc.center.z - bc.size.z / 2,
              zmax = bc.center.z + bc.size.z / 2;

        p2.x = Mathf.Clamp(p2.x, xmin, xmax);
        p2.y = Mathf.Clamp(p2.y, ymin, ymax);
        p2.z = Mathf.Clamp(p2.z, zmin, zmax);

        return t.TransformPoint(p2);
    }
}
