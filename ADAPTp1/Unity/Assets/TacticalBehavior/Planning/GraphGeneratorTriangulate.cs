using UnityEngine;
using System.Collections.Generic;

public class GraphGeneratorTriangulate : GraphGenerator
{
    public MeshFilter meshFilter;
    public MeshFilter meshFilterGraph;

    public void Start()
    {
        Vector3[] vertices;
        int[] indices;
        NavMesh.Triangulate(out vertices, out indices);

        // Create the NavGraph
        Graph = new NavGraph(vertices, indices);

        // Create a new Mesh to visualize the NavMesh
        var mesh = new Mesh() { name = "NavMeshVis" };
        mesh.vertices = vertices;
        mesh.triangles = indices;
        if (meshFilter != null) {
            meshFilter.mesh = mesh;
        }
        // Create a new Mesh to visualize the NavGraph
        var meshGraph = Graph.Mesh;
        if (meshFilterGraph != null) {
            meshFilterGraph.mesh = meshGraph;
        }
    }
}
