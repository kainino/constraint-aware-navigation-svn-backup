using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GraphGeneratorGrid : GraphGenerator
{
    public BoxCollider navBounds;
    /// <summary>
    /// The grid separation. MUST be less than NavMesh bake radius / sqrt3!
    /// </summary>
    public float gridSeparation;
    // 2 * sqrt3 * gridSeparation (max transition dist) < bake radius * 2

    /// <summary>
    /// Chebyshev radius of immediate neighbors.
    /// </summary>
    public int neighborRadius = 1;

    public virtual void Start()
    {
        var tr = navBounds.transform;
        var min = tr.TransformPoint(navBounds.center - navBounds.size / 2);
        var max = tr.TransformPoint(navBounds.center + navBounds.size / 2);
        // Assuming there is no rotation in the transformation.
        var size = max - min;
        int xnodes = (int) (size.x / gridSeparation);
        int ynodes = (int) (size.y / gridSeparation);
        int znodes = (int) (size.z / gridSeparation);

        var nodes = new NavGraphNode[xnodes, ynodes, znodes];

        // Maximum distance of the closest grid point to any NavMesh point. - KN
        float maxdist = gridSeparation * Mathf.Sqrt(3) / 2;

        // Insert nodes where appropriate (near the NavMesh).
        for (int x = 0; x < xnodes; x++) {
            for (int y = 0; y < ynodes; y++) {
                for (int z = 0; z < znodes; z++) {
                    var p = new Vector3(
                        min.x + x * gridSeparation,
                        min.y + y * gridSeparation,
                        min.z + z * gridSeparation);
                    NavMeshHit hit;
                    var n = nodes[x, y, z];
                    if (NavMesh.SamplePosition(p, out hit, maxdist, -1) &&
                        (n == null ||
                        Vector3.Distance(p, hit.position) <
                            Vector3.Distance(p, n.Position))) {
                        nodes[x, y, z] = new NavGraphGridNode(hit.position);
                    }
                    // Should this somehow test only the y distance?
                    // We originally said it would, but I realized that
                    // SamplePosition won't go only in y.
                    // I think this is fine. - KN
                }
            }
        }

        // Fill the adjacency lists for all of the nodes.
        for (int x = 0; x < xnodes; x++) {
            for (int y = 0; y < ynodes; y++) {
                for (int z = 0; z < znodes; z++) {
                    var n = nodes[x, y, z];
                    if (n == null) {
                        continue;
                    }
                    // Up to 26 nodes surrounding this one.
                    for (int dx = -neighborRadius; dx <= neighborRadius; dx++) {
                        int x1 = x + dx;
                        for (int dy = -neighborRadius; dy <= neighborRadius; dy++) {
                            int y1 = y + dy;
                            for (int dz = -neighborRadius; dz <= neighborRadius; dz++) {
                                int z1 = z + dz;
                                if ((dx == 0 && dy == 0 && dz == 0) ||
                                    x1 < 0 || x1 >= xnodes ||
                                    y1 < 0 || y1 >= ynodes ||
                                    z1 < 0 || z1 >= znodes) {
                                    continue;
                                }
                                AddAdjacency(n, nodes[x1, y1, z1]);
                            }
                        }
                    }
                }
            }
        }

        Graph = new NavGraph(nodes.Cast<NavGraphNode>().Where(n => n != null));
    }

    static void AddAdjacency(NavGraphNode n, NavGraphNode adj)
    {
        // Make sure that there is an adjacency here
        if (adj != null) {
            // And that it is not off the mesh
            if (TransitionOnMesh(n, adj)) {
                n.Adjacencies.Add(adj);
            }
        }
    }

    public static bool TransitionOnMesh(DefaultState n, DefaultState adj) {
        bool onmesh = true;
        for (float s = 0.25f; s < 0.99f; s += 0.25f) {
            var mp = n.statePosition()
                + (adj.statePosition() - n.statePosition()) * s;
            NavMeshHit hit;
            NavMesh.SamplePosition(mp, out hit, 0.1f, -1);
            onmesh &= hit.hit;
        }
        return onmesh;
    }
}
