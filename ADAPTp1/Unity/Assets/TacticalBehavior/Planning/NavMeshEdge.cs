using UnityEngine;
using System.Collections;

/// <summary>
/// An edge in the mesh (the value of a node in the NavGraph structure).
/// </summary>
public struct NavMeshEdge
{
    readonly Vector3[] vertices;
    public readonly int vert1;
    public readonly int vert2;

    public NavMeshEdge(ref Vector3[] verts, int v1, int v2)
    {
        vertices = verts;
        if (v1 < v2) {
            vert1 = v1;
            vert2 = v2;
        } else {
            vert1 = v2;
            vert2 = v1;
        }
    }

    public Vector3 GetMidpoint()
    {
        return (vertices[vert1] + vertices[vert2]) / 2;
    }

    public override bool Equals(object o)
    {
        return o is NavMeshEdge && Equals((NavMeshEdge) o);
    }

    public bool Equals(NavMeshEdge o)
    {
        return o.vert1 == vert1 && o.vert2 == vert2;
    }

    public override int GetHashCode()
    {
        return (vert1 << 16 & vert1 >> 16) ^ vert2;
    }
}