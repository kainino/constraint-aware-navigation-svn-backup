using UnityEngine;
using System.Collections.Generic;

public class GraphGeneratorHybrid : GraphGeneratorGrid
{
    public GraphGenerator other;

    public override void Start()
    {
        base.Start();
        //var tris = new GraphGeneratorTriangulate();
        //tris.Start();
        // TODO: Optimize?
        // Map triangulate-graph onto grid-graph.
        var tristogrid = new Dictionary<NavGraphNode, NavGraphNode>();
        foreach (var n in other.Graph.Nodes) {
            var nn = Graph.NearestNode(n.Position);
            tristogrid[n] = nn;
        }
        // Copy adjacencies into grid-graph.
        foreach (var n in other.Graph.Nodes) {
            var ttgn = tristogrid[n];
            foreach (var a in n.Adjacencies) {
                var ttga = tristogrid[a];
                if (ttgn != ttga && TransitionOnMesh(ttgn, ttga)) {
                    ttgn.Adjacencies.Add(ttga);
                }
            }
        }
    }
}
