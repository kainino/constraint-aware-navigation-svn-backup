using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class NavGraph
{
    public Mesh Mesh = null;

    public IEnumerable<NavGraphNode> Nodes { get; private set; }

    /// <summary>
    /// Constructs a NavGraph from an existing set of nodes with precomputed
    /// adjacency lists.
    /// </summary>
    public NavGraph(IEnumerable<NavGraphNode> ns)
    {
        Nodes = new HashSet<NavGraphNode>(ns);
    }

    /// <summary>
    /// Constructs a NavGraph from edges in a mesh triangulation.
    /// </summary>
    public NavGraph(Vector3[] verts, int[] indices)
    {
        var nodedict = new Dictionary<NavMeshEdge, NavGraphNode>();
        var adjs = new Dictionary<NavMeshEdge, List<NavMeshEdge>>();
        var mverts = new List<Vector3>();
        // It seems that the navmesh is not actually fully connected.
        var dedup = new Dictionary<Vector3, int>();

        // Find all of the (deduplicated) adjacencies
        for (int i = 0; i < indices.Length; i += 3) {
            int v1 = DedupAdd(dedup, verts, indices[i + 0]);
            int v2 = DedupAdd(dedup, verts, indices[i + 1]);
            int v3 = DedupAdd(dedup, verts, indices[i + 2]);
            NavMeshEdge e1 = new NavMeshEdge(ref verts, v1, v2);
            NavMeshEdge e2 = new NavMeshEdge(ref verts, v2, v3);
            NavMeshEdge e3 = new NavMeshEdge(ref verts, v3, v1);
            AdjAdd(adjs, e1, e2, e3);
            AdjAdd(adjs, e2, e3, e1);
            AdjAdd(adjs, e3, e1, e2);
            mverts.Add(e1.GetMidpoint());
            mverts.Add(e2.GetMidpoint());
            mverts.Add(e3.GetMidpoint());
        }

        //var annotations = GameObject.FindGameObjectsWithTag("NavAnnotations");

        // Construct the (deduplicated) map of nodes
        foreach (var kvp in adjs) {
            //var m = kvp.Key.GetMidpoint();
            //var a = annotations.Where(g =>
            //    (g.collider as BoxCollider).Contains(m, 0.1f));

            nodedict[kvp.Key] = new EdgeNode(kvp.Key/*, a*/);
        }
        // Set all of the node adjacencies
        foreach (var kvp in adjs) {
            foreach (var v in kvp.Value) {
                nodedict[kvp.Key].Adjacencies.Add(nodedict[v]);
            }
        }

        // Keep a collection of all of the Nodes
        Nodes = nodedict.Values;
        // Create the graph debug mesh
        Mesh = new Mesh() {
            name = "NavGraphVis",
            vertices = mverts.ToArray(),
            triangles = Enumerable.Range(0, mverts.Count).ToArray()
        };
    }

    static int DedupAdd(Dictionary<Vector3, int> dedup, Vector3[] verts, int i)
    {
        Vector3 v = verts[i];
        // Perhaps we already have this exact point...
        int index;
        if (dedup.TryGetValue(v, out index)) {
            return index; // if so, return.
        }
        // Perhaps we have a very similar point...
        foreach (var kvp in dedup) {
            if (Vector3.Distance(v, kvp.Key) < 0.01f) {
                return kvp.Value; // if so, return.
            }
        }
        // This must be a new point. Add it...
        dedup.Add(v, i);
        return i; // and return it.
    }

    static void AdjAdd(Dictionary<NavMeshEdge, List<NavMeshEdge>> adjs,
        NavMeshEdge e, params NavMeshEdge[] neighbors)
    {
        List<NavMeshEdge> s;
        if (!adjs.TryGetValue(e, out s)) {
            adjs.Add(e, s = new List<NavMeshEdge>(4));
        }
        foreach (var neighbor in neighbors) {
            s.Add(neighbor);
        }
    }

    public NavGraphNode NearestNode(Vector3 pos)
    {
        // TODO: totally inefficient
        float mindist = float.PositiveInfinity;
        NavGraphNode mindistnode = null;
        foreach (var n in Nodes) {
            var d = Vector3.Distance(pos, n.Position);
            if (d < mindist) {
                mindist = d;
                mindistnode = n;
            }
        }
        return mindistnode;
    }

    public IEnumerable<NavGraphNode> NearestNodes(BoxCollider c) {
        // TODO: totally inefficient
        return Nodes.Where(n =>
            Vector3.Distance(n.Position, c.ClosestPoint(n.Position)) < 0.001f);
    }
}
