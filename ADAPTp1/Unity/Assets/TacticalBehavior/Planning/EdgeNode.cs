using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// A node in the NavGraph structure at an edge midpoint
/// </summary>
public class EdgeNode : NavGraphNode
{
    private NavMeshEdge edge;

    //public EdgeNode(NavMeshEdge e, IEnumerable<GameObject> annot) : base(annot)
    public EdgeNode(NavMeshEdge e)
    {
        edge = e;
    }

    public override Vector3 Position {
        get { return edge.GetMidpoint(); }
    }
}
