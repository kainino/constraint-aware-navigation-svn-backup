using UnityEngine;
using System.Collections;

class NavGraphAction : DefaultAction
{
    public Vector3 direction;

    public NavGraphAction(DefaultState _from, DefaultState _to)
    {
        if (_from == null)
            Debug.Log("_from == null");
        var f = _from.statePosition();
        var t = _to.statePosition();

        this.direction = t - f;
        this.cost = Vector3.Distance(f, t); // this is the ESTIMATED cost.
        this.state = _to;
    }
}