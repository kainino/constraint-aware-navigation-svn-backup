using UnityEngine;
using System.Collections;

public class NavGraphGridNode : NavGraphNode
{
    Vector3 position;

    public NavGraphGridNode(Vector3 pos)
    {
        position = pos;
    }

    public override Vector3 Position {
        get { return position; }
    }
}
