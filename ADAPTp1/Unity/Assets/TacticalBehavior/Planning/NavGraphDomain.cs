using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class NavGraphDomain : PlanningDomainBase
{
    //private int layer = (1 << LayerMask.NameToLayer("Obstacles"));
    // | (1 << LayerMask.NameToLayer("StaticWorld"));

    public NavGraphDomain()
    {
    }

    public override DefaultAction generateAction(DefaultState p, DefaultState n)
    {
        //throw new System.NotImplementedException();
        //return new BestFirstAction(p, n);
        return new NavGraphAction(p, n);
    }

    public override float estimateTotalCost(
        ref DefaultState curr, ref DefaultState goal, float currentg)
    {
        float h = ComputeHEstimate(curr, goal);
        float f = currentg + h;
        return f;
    }

    public override bool isAGoalState(ref DefaultState s, ref DefaultState goal)
    {
        return ColliderState.Distance(s, goal) < 0.1f;
    }

    public override float evaluateDomain(ref DefaultState state)
    {
        return 1.0f;
    }

    public override float ComputeHEstimate(DefaultState _from, DefaultState _to)
    {
        // This multiplier makes the planner NON-OPTIMAL. -KN
        return ColliderState.Distance(_from, _to);// * TacBeh.MultiplierBase;
    }

    public override float ComputeGEstimate(DefaultState _from, DefaultState _to)
    {
        throw new System.NotImplementedException();
    }

    public override bool equals(DefaultState s1, DefaultState s2, bool isStart)
    {
        return s1 == s2 || ColliderState.Distance(s1, s2) < 0.001f;
    }

    public override void generateNeighbors(
        DefaultState currentState, ref List<DefaultState> neighbors)
    {
        var state = currentState as EdgeNode;
        neighbors.Clear();
        neighbors.AddRange(state.Adjacencies.Cast<DefaultState>());

        // TODO: figure out if we need any of this stuff - KN
#if false
        BestFirstState ACurrentState = currentState as BestFirstState;

        bool[] transitionsPossible = new bool[8];

        // doing non-diagonals first
        for (int i =0; i < transitionsList.Count; i+=2) {
            Collider [] colliders = Physics.OverlapSphere(ACurrentState.state + transitionsList[i], 0.25F, layer);

            //if (! Physics.CheckSphere(ACurrentState.state + transitionsList[i],0.5F,layer))
            if (colliders.Count() == 0) {
                transitionsPossible[i] = true;
                BestFirstState neighborState = new BestFirstState(ACurrentState.state + transitionsList[i]);
                neighbors.Add(neighborState);
            } else {
                transitionsPossible[i] = false;
            }

        }
        // diagonals
        for (int i =1; i < transitionsList.Count; i+=2) {
            Collider [] colliders = Physics.OverlapSphere(ACurrentState.state + transitionsList[i], 0.25F, layer);
            //if (! Physics.CheckSphere(ACurrentState.state + transitionsList[i],0.5F,layer))
            if (colliders.Count() == 0) {
                if (transitionsPossible[i - 1] == true || transitionsPossible[(i + 1) % transitionsList.Count] == true) {
                    transitionsPossible[i] = true;
                    BestFirstState neighborState = new BestFirstState(ACurrentState.state + transitionsList[i]);
                    neighbors.Add(neighborState);
                } else
                    transitionsPossible[i] = false;
            } else {
                transitionsPossible[i] = false;
            }

        }
#endif
    }

    public override void generateTransitions(
        ref DefaultState curr, ref DefaultState prev, ref DefaultState goal,
        ref List<DefaultAction> transitions)
    {
        var s = curr as NavGraphNode;

        var actions = s.Adjacencies.Select(e =>
            new NavGraphAction(s, e) as DefaultAction);
        //var actions = s.Adjacencies.Select(e => new DefaultAction() {
        //    cost = ColliderState.Distance(s, e), state = e });
        if (transitions.Count > 0) {
            transitions.Clear();
        }
        transitions.AddRange(actions);

        // TODO: Prune transitions to avoid dynamic hard obstacles - KN

        // TODO: figure out if we need any of this stuff - KN
#if false
        BestFirstState ACurrentState = currentState as BestFirstState;

        bool[] transitionsPossible = new bool[8];

        // doing non-diagonals first
        for (int i =0; i < transitionsList.Count; i+=2) {
            Collider [] colliders = Physics.OverlapSphere(ACurrentState.state + transitionsList[i], 0.25F, layer);

            //if (! Physics.CheckSphere(ACurrentState.state + transitionsList[i],0.5F,layer))
            if (colliders.Count() == 0) {
                transitionsPossible[i] = true;

                BestFirstAction action = new BestFirstAction();
                action.cost = Vector3.Distance(ACurrentState.state, ACurrentState.state + transitionsList[i]);
                action.direction = transitionsList[i];
                BestFirstState st = new BestFirstState(ACurrentState.state + transitionsList[i]);
                action.state = st;
                transitions.Add(action);

            } else {
                transitionsPossible[i] = false;

            }

        }
        // diagonals
        for (int i =1; i < transitionsList.Count; i+=2) {
            Collider [] colliders = Physics.OverlapSphere(ACurrentState.state + transitionsList[i], 0.25F, layer);
            //if (! Physics.CheckSphere(ACurrentState.state + transitionsList[i],0.5F,layer))
            if (colliders.Count() == 0) {
                if (transitionsPossible[i - 1] == true || transitionsPossible[(i + 1) % transitionsList.Count] == true) {
                    transitionsPossible[i] = true;
                    BestFirstAction action = new BestFirstAction();
                    action.cost = Vector3.Distance(ACurrentState.state, ACurrentState.state + transitionsList[i]);
                    action.direction = transitionsList[i];
                    BestFirstState st = new BestFirstState(ACurrentState.state + transitionsList[i]);
                    action.state = st;
                    transitions.Add(action);
                } else
                    transitionsPossible[i] = false;
            } else {
                transitionsPossible[i] = false;
            }

        }
#endif
    }

    public override void generatePredecessors(DefaultState currentState, ref List<DefaultAction> actionList)
    {
        DefaultState p = null, g = null;
        generateTransitions(ref currentState, ref p, ref g, ref actionList);
        // TODO: Prune actionList to avoid dynamic hard obstacles - KN
    }
}
