using UnityEngine;
using System.Collections;

public abstract class GraphGenerator : MonoBehaviour
{
    public NavGraph Graph { get; protected set; }

    void OnDrawGizmos()
    {
        if (Graph == null || Graph.Nodes == null) {
            return;
        }
        foreach (var n in Graph.Nodes) {
            Gizmos.color = Color.blue;
            foreach (var a in n.Adjacencies) {
                Gizmos.DrawLine(n.Position, a.Position);
            }
            //Gizmos.DrawCube(n.Position, new Vector3(0.2f, 0.2f, 0.2f));
        }
    }
}
