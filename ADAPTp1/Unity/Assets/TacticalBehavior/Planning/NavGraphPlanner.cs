using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

class NavGraphPlanner : BestFirstSearchPlanner
{
    public TacBeh Beh { get; set; }

    protected override float ComputeCostMult(DefaultState fr, DefaultState to)
    {
        var fp = fr.statePosition();
        var tp = to.statePosition();
        return Beh.MultiplierField((fp + tp) / 2f);
    }

    public void VisualizeField(MeshFilter mf)
    {
        var t = mf.transform;
        var m = mf.mesh;
        var vs = m.vertices;
        // calculate mult field - KN
        for (int i = 0; i < vs.Length; i++) {
            var v = t.TransformPoint(vs[i]);
            vs[i].y = Beh.MultiplierField(new Vector3(v.x, t.position.y, v.z));
        }
        m.vertices = vs;
    }
}
