using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public partial struct TacBeh {
    [System.Serializable]
    public class Constraint {
        //const float InsigMultDiff = 0.5f;

        public ConstrPrep prep;
        public Cardinal dir;
        public Transform obj;
        public float weight;

        /// <summary>
        /// Approximate radius of (strong) influence.
        /// </summary>
        public float InfluenceRadius {
            get {
                if (prep == ConstrPrep.In) return 0.1f;
                return Mathf.Abs(weight) * 2;
            }
        }

        public BoxCollider Annotation {
            get {
                var o = obj.GetComponent<Annotations>();
                if (o == null) return obj.collider as BoxCollider;

                if (prep == ConstrPrep.None) {
                    return null;
                } else if (prep == ConstrPrep.In || prep == ConstrPrep.Near) {
                    if (dir == Cardinal.None) return o.Near;
                    else if (dir == Cardinal.BackOf) return o.Back;
                    else if (dir == Cardinal.FrontOf) return o.Front;
                    else if (dir == Cardinal.LeftOf) return o.Left;
                    else if (dir == Cardinal.RightOf) return o.Right;
                }
    
                return o.collider as BoxCollider;
            }
        }

        public override string ToString()
        {
            return string.Format("({0} {1} {2} w={3})",
                prep, dir == Cardinal.None ? "" : dir.ToString(),
                obj.name, weight);
        }

        //Dictionary<DefaultState, float> lastupdate;
        IEnumerable<DefaultState> lastupdate;
        DefaultState laststate;
        public void FindNodesToUpdate(NavGraph gr,
            out IEnumerable<DefaultState> changed) {
            var state = gr.NearestNode(obj.position);
            // apparently isStatic is an editor-only property? -KN
            if (obj.gameObject.isStatic || state == laststate) {
                changed = null;
                return;
            }
            laststate = state;
            
            var pos = obj.position;
            var r = InfluenceRadius;

            // TODO: Make sure these queries aren't getting exhausted in one
            // place and breaking usage in other places.
            var influenced = gr.Nodes.Where(n =>
                Annotation.MinDistance(n.Position) < r).Cast<DefaultState>();
            changed = lastupdate == null ? influenced :
                influenced.Union(lastupdate);
            lastupdate = influenced;
            //changed = lastupdate == null ? influenced :
            //    influenced.Where(s =>
            //        Mathf.Abs(lastupdate[s] - MultField(s)) < InsigMultDiff);
            //lastupdate = influenced.ToDictionary(s => s, s => MultField(s));
        }

        float MultField(DefaultState s)
        {
            return MultiplierField(s.statePosition());
        }

        public float MultiplierField(Vector3 mp)
        {
            if (Annotation == null) {
                return 0f;
            }
#if false // Using annotations with hard edges, ignoring goal.
            const float Default = 10;
            float mult = Default;
            if (Constr != TacBeh.ConstrPrep.None) {
                if (ConstrAnnotation.Contains(mp, 0.1f)) {
                    mult /= ConstrWeight;
                }
            }
#elif false // Using annotations with soft edges, ignoring goal.
            const float Default = 2f;
            float mult = Default;
            if (Constr != TacBeh.ConstrPrep.None) {
                var d = Vector3.Distance(mp, ConstrAnnotation.ClosestPoint(mp));
                mult = Mathf.Pow(Default, ConstrWeight * d);
            }
#elif false // Using annotations with soft edges, ignoring goal. V2.
            const float Default = 1f;
            float mult = Default;
            if (Constr != TacBeh.ConstrPrep.None) {
                var d = Vector3.Distance(mp, ConstrAnnotation.ClosestPoint(mp));
                mult *= Mathf.Pow(d, ConstrWeight);
            }
#elif true // Using annotations with soft edges, ignoring goal. V3.
            const float k1 = 0.4f; // makes the dip less sharp
            const float k2 = 0.5f; // makes the dip have a larger radius

            var r = Vector3.Distance(mp, Annotation.ClosestPoint(mp));
            float mult = 0;
            if ((prep == TacBeh.ConstrPrep.In && r <= 0.001f) ||
                prep == TacBeh.ConstrPrep.Near && r <= InfluenceRadius) {
                mult = -weight * Mathf.Pow(k1 + k2 * r, -2);
            }
#elif false // Using distance to constraint center, ignoring goal.
            var gp = GoalAnnotation.transform.position;
    
            float mult = 0f; //Vector3.Distance(gp, mp);
            if (Constr != TacBeh.ConstrPrep.None) {
                var cp = ConstrAnnotation.transform.position;
                var d = Vector3.Distance(cp, mp);
                mult += d;
                //mult = Math.Min(mult, Vector3.Distance(cp, mp));
                //mult = (ConstrWeight * Vector3.Distance(cp, mp) + mult) /
                //    (ConstrWeight + 1);
            }
#elif false // NON-WORKING Using distance to constraint and goal centers.
            var gp = GoalAnnotation.transform.position;
    
            var ddistG = Vector3.Distance(tp, gp) - Vector3.Distance(fp, gp);
            var ratioG = Vector3.Distance(tp, gp) / Vector3.Distance(fp, gp);
    
            float exp = ddistG / Vector3.Distance(tp, gp);
            if (Constr != TacBeh.ConstrPrep.None) {
                var cp = ConstrAnnotation.transform.position;
                var ddistC = Vector3.Distance(tp, cp) - Vector3.Distance(fp, cp);
                var ratioC = Vector3.Distance(tp, cp) / Vector3.Distance(fp, cp);
                exp += ddistC / Vector3.Distance(tp, cp);
            }
            float mult = Mathf.Pow(2, exp);
#elif false // NON-WORKING Using smaller weights perpendicular to constraint.
            var dir = (tp - fp).normalized;
            float mult = 1f;
            if (Constr != TacBeh.ConstrPrep.None) {
                var perp = (mp - ConstrAnnotation.ClosestPoint(mp)).normalized;
                mult /= Mathf.Abs(Vector3.Dot(dir, perp));
            }
#endif
            return mult;
        }
    }
}
