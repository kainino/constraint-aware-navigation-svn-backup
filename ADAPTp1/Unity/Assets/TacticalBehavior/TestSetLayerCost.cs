using UnityEngine;
using System.Collections;

public class TestSetLayerCost : Behavior
{
    NavMeshAgent nma;

    void Start()
    {
        nma = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        nma.SetLayerCost(
            NavMesh.GetNavMeshLayerFromName("TestLayer"),
            Input.GetKey(KeyCode.LeftShift) ? 1e9f : 1f);
    }
}
