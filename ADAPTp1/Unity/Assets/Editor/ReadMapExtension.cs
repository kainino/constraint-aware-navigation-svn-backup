using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Runtime.InteropServices;


[CustomEditor(typeof(ReadMap))]
public class ReadMapExtension : Editor {
	ReadMap mainScript;
	string file_name;
	GameObject ObstaclePrefab;
	private int rows, columns;
	
	public override void OnInspectorGUI()
	{
		GUILayout.BeginVertical();
		mainScript.maze_filename = file_name = EditorGUILayout.TextField("Maze", mainScript.maze_filename);
		mainScript.ObstaclePrefab = ObstaclePrefab = (GameObject)EditorGUILayout.ObjectField("Obstacle", mainScript.ObstaclePrefab, typeof(GameObject));
		
		if (GUILayout.Button("Create Map"))
		{
			destroyMap();
			createMap();
		}
		
		GUILayout.EndVertical();
	}
	
	public void OnEnable()
	{
		mainScript = (ReadMap)target;	
	}
	
	void destroyMap()
	{
		foreach (GameObject obstacle in GameObject.FindGameObjectsWithTag("movable obstacles")) {
			GameObject.DestroyImmediate(obstacle);	
		}

        GameObject plane = GameObject.FindGameObjectWithTag("plane");
        GameObject.DestroyImmediate(plane);
	}
	
	void createMap()
	{
		System.IO.StreamReader file = new System.IO.StreamReader("Assets/Mazes/" + file_name);
		string line;
		int row = 0;
		while ((line = file.ReadLine()) != null)
		{
			 if (line.Contains("height")) {
				line = line.Substring(7, line.Length - 7);
				rows = int.Parse(line);
			} else if (line.Contains("width")) {
				line = line.Substring(6, line.Length - 6);
				columns = int.Parse(line); 
			} else if (line.StartsWith("@") || line.StartsWith(".")) {
				char[] charLine = line.ToCharArray();
				for (int column = 0; column < line.Length; column++) {
					if (charLine[column] == '@') {
						Instantiate(ObstaclePrefab, new Vector3(column, 0.5f, (rows-1)-row), Quaternion.identity);
					} 
				}
				row++;
			}
		}
		file.Close();

        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.tag = "plane";
        plane.transform.localScale = new Vector3((float)rows/5, 1.0f, (float)columns/5);
        plane.transform.Translate(new Vector3((float)rows/2, -1.0f, (float)columns/2));
	}
}
