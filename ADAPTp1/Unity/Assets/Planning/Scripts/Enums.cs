//! Enum to determine Path Status
public enum PathStatus
{
	NoPath,
	Incomplete,
	SubOptimal,
	Optimal
};

//! Enum used for debugging purposes only
public enum ContainerType
{
	Open,
	Close,
	Incons,
	Plan,
	Visited
};
